﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arcive_Drive.Enc_AND_Dec
{
    class Base16
    {
        private static char[] HEX = new char[]{
        '0', '1', '2', '3', '4', '5', '6', '7',
        '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };

        /**
         * Convert bytes to a base16 string.
         */
        public static String encode(byte[] byteArray)
        {
            System.Text.StringBuilder hexBuffer = new System.Text.StringBuilder(byteArray.Length * 2);
            for (int i = 0; i < byteArray.Length; i++)
                for (int j = 1; j >= 0; j--)
                    hexBuffer.Append(HEX[(byteArray[i] >> (j * 4)) & 0xF]);
            return hexBuffer.ToString();
        }

        /*
         * Convert a base16 string into a byte array.
         */
        public static byte[] decode(String s)
        {
            int len = s.Length;
            byte[] r = new byte[len / 2];
            for (int i = 0; i < r.Length; i++)
            {
                int digit1 = s.ToCharArray()[i * 2], digit2 = s.ToCharArray()[i * 2 + 1];
                if (digit1 >= '0' && digit1 <= '9')
                    digit1 -= '0';
                else if (digit1 >= 'a' && digit1 <= 'f')
                    digit1 -= 'a' - 10;
                if (digit2 >= '0' && digit2 <= '9')
                    digit2 -= '0';
                else if (digit2 >= 'a' && digit2 <= 'f')
                    digit2 -= 'a' - 10;

                r[i] = (byte)((digit1 << 4) + digit2);
            }
            return r;
        }
    }
}
