﻿using System;
using System.IO;
using System.Text;
namespace Arcive_Drive{

    public class Run_From_TXT{
        public int en_de = 0;
        public string loc = "";
        public string filename = "";
        public string diagnosis = "";
        public int thread_num = 0;

        public Run_From_TXT(){
            string[] midi = { };
            try{
                FileStream tmp = new FileStream("set.txt", FileMode.Open, FileAccess.Read);
                byte[] file = new byte[tmp.Length];
                tmp.Read(file, 0, (int)tmp.Length);
                Console.WriteLine(Encoding.ASCII.GetString(file));
                midi = Encoding.ASCII.GetString(file).Split('\n');
                en_de = int.Parse(midi[0]);
                loc = midi[1]; 
                filename = midi[2];
                diagnosis = midi[3];
                thread_num = int.Parse(midi[4]);
                
            }
            catch (IOException e){
                Console.Write(e.StackTrace);
                Console.Write(" Enter 0 for File - Encryption... " +
                "\n" + " Enter 1 For File - Decryption..." + "\n");
                en_de = int.Parse(Console.ReadLine());

                Console.WriteLine("Enter The file Location: ");
                loc = Console.ReadLine();
                Console.WriteLine("Enter The file Name: ");
                filename = Console.ReadLine();

                Console.WriteLine("Type How Many Threads: ");
                thread_num = int.Parse(Console.ReadLine());
                Console.WriteLine("Diag - mode [on / off]: ");
                diagnosis = Console.ReadLine();


            }
        }
    }
}
