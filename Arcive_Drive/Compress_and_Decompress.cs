﻿using System;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Collections;

namespace Arcive_Drive
{
    class Compress_and_Decompress
    {
        public static void Comm(byte[] file_in, int wt)
        {
            string dstFilenamewithpath = "";
            FileStream fsInFile = null;
            FileStream fsOutFile = null;
            GZipStream Myrar = null;
            byte[] filebuffer;
            int count = 0;
            if (wt == 0)
            {
                try
                {

                    dstFilenamewithpath = Encoding.ASCII.GetString(file_in) + ".rar";

                    fsOutFile = new FileStream(dstFilenamewithpath, FileMode.Create, FileAccess.Write, FileShare.None);
                    Myrar = new GZipStream(fsOutFile, CompressionMode.Compress, true);
                    fsInFile = new FileStream("AR.enA", FileMode.Open, FileAccess.Read, FileShare.Read);
                    filebuffer = new byte[fsInFile.Length];
                    count = fsInFile.Read(filebuffer, 0, filebuffer.Length);
                    fsInFile.Close();
                    fsInFile = null;
                    Myrar.Write(filebuffer, 0, filebuffer.Length);
                }

                catch (Exception ex)
                {
                    System.Diagnostics.Debug.Assert(false, ex.ToString());
                }

                // Releasing objects
                finally
                {
                    if (Myrar != null)
                    {
                        Myrar.Close();
                        Myrar = null;
                    }
                    if (fsOutFile != null)
                    {
                        fsOutFile.Close();
                        fsOutFile = null;
                    }
                    if (fsInFile != null)
                    {
                        fsInFile.Close();
                        fsInFile = null;
                    }
                }
            }
            else
            {
                try
                {
                    dstFilenamewithpath = "midi.file";

                    fsOutFile = new FileStream(dstFilenamewithpath, FileMode.Create, FileAccess.Write, FileShare.None);
                    Myrar = new GZipStream(fsOutFile, CompressionMode.Compress, true);
                    // fsInFile = new FileStream("AR.enA", FileMode.Open, FileAccess.Read, FileShare.Read);
                    Myrar.Write(file_in, 0, file_in.Length);

                }

                catch (Exception ex)
                {
                    System.Diagnostics.Debug.Assert(false, ex.ToString());
                }

                // Releasing objects
                finally
                {
                    if (Myrar != null)
                    {
                        Myrar.Close();
                        Myrar = null;
                    }
                    if (fsOutFile != null)
                    {
                        fsOutFile.Close();
                        fsOutFile = null;
                    }
                    if (fsInFile != null)
                    {
                        fsInFile.Close();
                        fsInFile = null;
                    }
                }
            }
        }
        public static void Decc(string file_in, int wt,string file)
        {
            DateTime current;
            string dstFile = "";

            FileStream fsInFile = null;
            FileStream fsOutFile = null;
            GZipStream Myrar = null;
            const int bufferSize = 1024;
            byte[] buffer = new byte[bufferSize];
            int count = 0;

            try
            {
                current = DateTime.Now;

                if (wt == 0){
                    dstFile = "AR.enA";
                }

                else {
                    dstFile = file;
                }
                fsInFile = new FileStream(file_in + ".rar", FileMode.Open,FileAccess.Read, FileShare.Read);
                fsOutFile = new FileStream(dstFile, FileMode.Create,FileAccess.Write, FileShare.None);
                Myrar = new GZipStream(fsInFile,CompressionMode.Decompress, true);
                while (true)
                {
                    count = Myrar.Read(buffer, 0, bufferSize);
                    if (count != 0)
                    {
                        fsOutFile.Write(buffer, 0, count);
                    }
                    if (count != bufferSize)
                    {
                        // Have reached the end
                        Console.WriteLine("Done With Compration");
                        break;
                    }
                }
            }


            catch (Exception ex)
            {
                // Handle or display the error
                Console.WriteLine(ex.StackTrace);
                System.Diagnostics.Debug.Assert(false, ex.ToString());
              
                
            }
            finally
            {
                if (Myrar != null)
                {
                    Myrar.Close();
                    Myrar = null;
                }
                if (fsOutFile != null)
                {
                    fsOutFile.Close();
                    fsOutFile = null;
                }
                if (fsInFile != null)
                {
                    fsInFile.Close();
                    fsInFile = null;
                }
            }
        }
    }
}
